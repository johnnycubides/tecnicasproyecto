# Proyecto de Técnicas de Integración #

### Desarrolladores ###

* Iván Ricardo Aguilar Díaz.
* Jorge Leonardo Barbosa Rodriguez.
* Oscar David Becerra Pedraza.
* Johnny German Cubides Castro
* Francisco Eduardo Lopez Cely.
* Julian Andres Murcia Narvaez
* Elton Jhon Nova.
* Oscar Pineda .
* Sebastian Felipe Rojas Marulanda.


####Ayuda: Clonación, pull y push proyecto####

![bitbukect.png](https://bitbucket.org/repo/A47MGX/images/401378937-bitbukect.png)


## Instalar git desde un repositorio en Linux ##
Desde el gestor de paquetes:

```
# apt-get install git
```
o

```
$ sudo apt-get install git
```

Configuración Inicial de GIT:

```
$ git config --global user.name "nombre usuario"
$ git config --global user.email suUsuario@unal.edu.co
```
Selección de un editor de texto:

```
$ git config --global core.editor nano
```

Comprobando configuración
```
$ git config --list
```
Clonar el proyecto
```
$ mkdir directorioProyecto; cd directorioProyecto
$ git clone https://suUsuario@bitbucket.org/johnnycubides/tecnicasproyecto.git
```
## Enviar y recibir cambios del Proyecto ##

**Actualizar cambios** que se encuentran en el repositorio donde está alojado el proyecto, cambios que son realizados
por otros integrantes del equipo.
```
$ git pull origin master
```

**Subir cambios**, revisiones al repositorio remoto del proyecto
; hacer push (subir mis cambios) anexando **nuevos archivos** al repositorio remoto

```
$ git pull origin master
$ git add --all
$ git commit -m "Comentario sobre cambios"
$ git push origin master
```

Puede verificar el estado de sus cambios (En su repositorio local) usando el siguiente comando:
```
$ git status
```
El comando anterior como salida le retorna sugerencias que le facilitan la identificación del procedimiento a seguir.

## GIT para Windows ##

Descargar e instalar GIT con opciones de BASH (shell) de aquí:

https://git-for-windows.github.io/

Funcionan los mismos comandos que para linux

ver vídeo

https://www.youtube.com/watch?v=O72FWNeO-xY

Nota 1: No hacer la configuración del editor de texto nano, pues es podible que no exista, no hacer ese paso continuar al siguiente.