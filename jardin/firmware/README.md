#Programa que correrá el procesador LM32#

Como era de esperarse cada directorio en éste nivel contiene
programas para ejecutar por el procesador LM32

## Makefile ##

para crear cada programa archivo.o se usa las siguientes funciones del
*Makefile*

```
make clean
make
```
La primera limpia el directorio borrando el archivo objeto
la segunda instrucción crea el archivo objeto, si no lo hace
es debido a problemas de sintaxis o semánticos u otros.
