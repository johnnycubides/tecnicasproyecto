/*
#include "soc-hw.h"


int main(int argc, char **argv){
	
	uint32_t numa, numb, mostrar, divisor, resultado;
	uint8_t temp, indicador, operacion, numchar, i, bandera_error;

	uart_init();
	
	for(;;){

		numa=0;
		numb=0;
		mostrar=0;
		temp=0;
		indicador=0;
		operacion=0;
		divisor=1000000000;
		numchar=0;
		resultado=0;
		i=0;
		bandera_error=0;

		//Lee y crea el primer número y también el signo de la operación.

		uart_putstr("\r\nCalculadora de 32 bits. Ejemplo: 23+34(Enter)\r\n"); 
		do{
		
			temp=uart_getchar();
		
			if(temp>47 && temp<58){
				uart_putchar(temp);
				numa=numa*10+(temp-48);
			}
			if(temp==42 || temp==43 || temp==45 || temp==47){
				operacion=temp;
				uart_putchar(operacion);
				indicador=1;
			}
			if((temp<47 || temp>58) && temp!=13 && temp!=42 && temp!=43 && temp!=45 && temp!=47 ){
				uart_putstr("\r\nCaracter inválido. Repita el proceso\r\n");
				numa=0;
			}
			
		}
		while(indicador==0);

		indicador=0;

		//Lee y crea el segundo número.

		do{

			temp=uart_getchar();

			if(temp>47 && temp <58){
				uart_putchar(temp);
				numb=numb*10+(temp-48);
			}
			if(temp==13){
				indicador=1;
			}
			if((temp<47 || temp >58) && temp!=13){
				uart_putstr("\r\nCaracter inválido. Vuelva a ingresar el segundo número\r\n");
				numb=0;
			}			

		}
		while(indicador==0);

		indicador=0;

		//Realiza las operaciones matemáticas.
		
		switch(operacion){  

			case 45:
				if(numa>=numb){
					resultado=numa-numb;
					uart_putchar(61);
					uart_putchar(32);
				}
				else{
					uart_putstr("\r\nResultado negativo.\r\n");
					bandera_error=1;
				}
				break;
			case 43:
				resultado=numa+numb;
				uart_putchar(61);
				uart_putchar(32);
				break;
			case 42:
				resultado=numa*numb;
				uart_putchar(61);
				uart_putchar(32);
				break;
			case 47:
				if(numb==0){
					uart_putstr("\r\nError. División por cero\r\n");
					bandera_error=1;
				}
				else{
					resultado=numa/numb;
					uart_putchar(61);
					uart_putchar(32);
				}

				break;
			default:
				return 0;
				break;		

		} 
				
		for(i=0;i<10;i++){

			if(bandera_error==0){			

             			mostrar=resultado/divisor;   
             
             			if(mostrar<1 && mostrar>0){
                       			divisor=divisor/10;                             
             			}
	             		else{
					uart_putchar(mostrar+48);
                  			resultado=resultado-mostrar*divisor;
                  			divisor=divisor/10;     
             			}
             			if(divisor==1){
                        	    	uart_putchar(resultado+48);
                        	    	i=10;					
                		}
             
  			} 
			else{
				i=10;
			}    

		}
		
		uart_putstr("\r\nOperación terminada\r\n");
		
	}
	
}
*/

#include "soc-hw.h"

int main(int argc, char **argv){
	
//declaracion de variables, rx dato de recepcińn, tx dato de transmición
	uint8_t rx_data;
	uint8_t tx_data;

// inicialización de la uart
	uart_init();
	

	for(;;){

// inicializacion de las variables rx,tx		
		rx_data = 0;
		tx_data = 0;

// asignar a rx_data el valor obtenido en el puerto rx en ASCII
		rx_data = uart_getchar();
// devolver el valor de rx_data por el puerto serial tx en ASCII
		uart_putchar(rx_data);
		
// comparacion del valor de rx_data, (si rx_data=1)
		if(rx_data==49){
// asignar un valor a tx_data, tx_data=3
			tx_data=51;
		}
		else{
// asinar un valor a tx_data, tx_data=4
			tx_data=52;	
		}
	

// comparacion del valor de rx_data, (si rx_data=2)
		if(rx_data==50){
// asignar un valor a tx_data, tx_data=5
			tx_data=53;
		}
		else{
// asinar un valor a tx_data, tx_data=6
			tx_data=54;	
		}
// devolver el valor de tx_data por el puerto serial tx en ASCII
		uart_putchar(tx_data);
		
		if(rx_data==49){
			writeGpio(1);
		}else{
			writeGpio(0);		
		}
	}
}
