/*
|Titulo:
Author:

Descripcion

uart0 comunicacion serial entre LM32 y microcontrolador
uart1 comunicacion LM32 terminal
*/
#include "soc-hw.h"



//Funcion de impresion 
void imprimir(uint8_t x){
	uint8_t temp;
	temp = x/10;
	uart1_putchar(temp+48);
	temp = x % 10;
	uart1_putchar(temp+48);
}


int main(int argc, char **argv){

	uint32_t v[2];
	uint32_t temperatura, luminosidad;
	uint32_t GPIO32=0;
	uint32_t pir;
	int i=0;
	int opcion=0;

	do{

	uart1_putstr("\r\n ||BIENVENIDO AL MENU DE CONTROL ||\r\n");
	uart1_putstr("\r\n || a --> Automatico.            ||\r\n");
	uart1_putstr("\r\n || m --> Manual.                ||\r\n");
    uart1_putstr("\r\n || Ingrese opcion:              ||\r\n");
    opcion = uart1_getchar();

	}

	while(!(opcion == 'a' || opcion == 'm'));

	if(opcion == 'a'){
        for(;;){
            uart0_putchar(1); //Peticion al microcontrolador para hacer conversiones
            v[0] = uart0_getchar(); 	//Temperatura
            v[1] = uart0_getchar(); 	//Luminosidad
            pir=readGpio();

            temperatura=v[0]*500/1023;// funcion de coversion entero a a funcion de temperatura
            luminosidad=v[1];//luminosidad en % no se hace conversion

            uart1_putstr("\r\n temperatura\r\n");
            imprimir((uint8_t)(temperatura));
            uart1_putstr("\r\n luminosidad \r\n");
            imprimir((uint8_t)(luminosidad));
            uart1_putstr("\r\n movimiento \r\n");
            imprimir((uint8_t)(pir));

            sleep(1000); //espere un segundo

            if(temperatura>=30 && pir==1){ // condiciones para el control de servomotores

                                for(i=0; i<50; i++){
                                                    writeGpio(GPIO32+3);
                                                    sleep(1);
                                                    writeGpio(GPIO32);
                                                    sleep(19);
                                                   }
                                }

            else{
                for(i=0; i<50; i++){
                writeGpio(GPIO32+3);
                sleep(2);
                writeGpio(GPIO32);
                sleep(18);
                }
            }

            if(luminosidad<=20){// condiciones para control de leds
            GPIO32=60 ;// 1 1 1 1 0 0 = 60
            writeGpio(GPIO32);
            }
            else if((luminosidad>20) && (luminosidad<=40)){
                GPIO32=28;//0 1 1 1 0 0 = 28
                writeGpio(GPIO32) ;
            }
            else if((luminosidad>40) && (luminosidad<=60)){
                GPIO32=12 ;// 0 0 1 1 0 0 = 12
                writeGpio(GPIO32);
            }
            else if((luminosidad>60) && (luminosidad<=80)){
                GPIO32=4;// 0 0 0 1 0 0 = 4
                writeGpio(GPIO32);
            }
            else{
                GPIO32=0 ;
                writeGpio(GPIO32);
            }

        }
    }
    else{

        do{
            int temp=opcion;

            uart1_putstr("\r\n ||      MENU DE CONTROL MANUAl    ||\r\n");
            uart1_putstr("\r\n ||a-->  intensidad muy alta       ||\r\n");
            uart1_putstr("\r\n ||b-->  intensidad alta           ||\r\n");
            uart1_putstr("\r\n ||c-->  intensidad media          ||\r\n");
            uart1_putstr("\r\n ||d-->  intensidad baja           ||\r\n");
            uart1_putstr("\r\n ||e-->  intensidad nula           ||\r\n");
            uart1_putstr("\r\n ||f-->  abrir ventanas            ||\r\n");
            uart1_putstr("\r\n ||g-->  cerrar ventanas           ||\r\n");
            uart1_putstr("\r\n ||s-->  salir                     ||\r\n");
            opcion = uart1_getchar();





                switch(opcion){
                case 'a' :
                if (opcion==temp){
                                 uart1_putstr("\r\n ||la intensidad ya es muy alta||\r\n");
                                 sleep(1000);
                                 }
                else{
                     GPIO32=60 ;// 1 1 1 1 0 0 = 60 todas las luces encendidas
                     writeGpio(GPIO32);
                    }
                break;

                case 'b' :
                if (opcion==temp){
                                  uart1_putstr("\r\n ||la intensidad ya es alta ||\r\n");
                                  sleep(1000);
                                 }
                else{
                      GPIO32=28;//0 1 1 1 0 0 = 28 3 luces encendidas
                      writeGpio(GPIO32) ;
                    }
                break;

                case 'c' :
                if(opcion==temp){
                                 uart1_putstr("\r\n ||la intensidad ya es media ||\r\n");
                                 sleep(1000);
                                 }
                else{
                     GPIO32=12;//0 0 1 1 0 0 = 12
                     writeGpio(GPIO32);
                    }
                break;

                case 'd' :
                if(opcion==temp){
                                 uart1_putstr("\r\n ||la intensidad ya es baja ||\r\n");
                                 sleep(1000);
                                }
                else{
                     GPIO32=8;//0 0 0 1 0 0 = 8
                     writeGpio(GPIO32);
                    }
                break;

                case 'e' :
                if(opcion==temp){
                                uart1_putstr("\r\n ||la intensidad ya nula ||\r\n");
                                sleep(1000);
                                }
                else{
                     GPIO32=0;//0 0 0 1 0 0 = 8
                     writeGpio(GPIO32);
                    }

                break;

                case 'f' :
                if(opcion==temp){
                                uart1_putstr("\r\n ||la ventana ya esta abierta ||\r\n");
                                sleep(1000);
                                }
                else {
                      for(i=0; i<50; i++){
                                          writeGpio(GPIO32+3);
                                          sleep(1);
                                          writeGpio(GPIO32);
                                          sleep(19);
                                         }
                     }
                break;

                case 'g' :
                if(opcion==temp){
                                uart1_putstr("\r\n ||la ventana ya esta cerrada ||\r\n");
                                sleep(1000);
                                }
                else{
                     for(i=0; i<50; i++){
                                         writeGpio(GPIO32+3);
                                         sleep(2);
                                         writeGpio(GPIO32);
                                         sleep(18);
                                        }
                    }
                break;


            }

        }
        while(!(opcion =='s'));

    }

}
