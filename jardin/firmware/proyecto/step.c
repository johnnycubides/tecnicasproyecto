/*
#include "soc-hw.h"


int main(int argc, char **argv){
	
	uint32_t numa, numb, mostrar, divisor, resultado;
	uint8_t temp, indicador, operacion, numchar, i, bandera_error;

	uart_init();
	
	for(;;){

		numa=0;
		numb=0;
		mostrar=0;
		temp=0;
		indicador=0;
		operacion=0;
		divisor=1000000000;
		numchar=0;
		resultado=0;
		i=0;
		bandera_error=0;

		//Lee y crea el primer número y también el signo de la operación.

		uart_putstr("\r\nCalculadora de 32 bits. Ejemplo: 23+34(Enter)\r\n"); 
		do{
		
			temp=uart_getchar();
		
			if(temp>47 && temp<58){
				uart_putchar(temp);
				numa=numa*10+(temp-48);
			}
			if(temp==42 || temp==43 || temp==45 || temp==47){
				operacion=temp;
				uart_putchar(operacion);
				indicador=1;
			}
			if((temp<47 || temp>58) && temp!=13 && temp!=42 && temp!=43 && temp!=45 && temp!=47 ){
				uart_putstr("\r\nCaracter inválido. Repita el proceso\r\n");
				numa=0;
			}
			
		}
		while(indicador==0);

		indicador=0;

		//Lee y crea el segundo número.

		do{

			temp=uart_getchar();

			if(temp>47 && temp <58){
				uart_putchar(temp);
				numb=numb*10+(temp-48);
			}
			if(temp==13){
				indicador=1;
			}
			if((temp<47 || temp >58) && temp!=13){
				uart_putstr("\r\nCaracter inválido. Vuelva a ingresar el segundo número\r\n");
				numb=0;
			}			

		}
		while(indicador==0);

		indicador=0;

		//Realiza las operaciones matemáticas.
		
		switch(operacion){  

			case 45:
				if(numa>=numb){
					resultado=numa-numb;
					uart_putchar(61);
					uart_putchar(32);
				}
				else{
					uart_putstr("\r\nResultado negativo.\r\n");
					bandera_error=1;
				}
				break;
			case 43:
				resultado=numa+numb;
				uart_putchar(61);
				uart_putchar(32);
				break;
			case 42:
				resultado=numa*numb;
				uart_putchar(61);
				uart_putchar(32);
				break;
			case 47:
				if(numb==0){
					uart_putstr("\r\nError. División por cero\r\n");
					bandera_error=1;
				}
				else{
					resultado=numa/numb;
					uart_putchar(61);
					uart_putchar(32);
				}

				break;
			default:
				return 0;
				break;		

		} 
				
		for(i=0;i<10;i++){

			if(bandera_error==0){			

             			mostrar=resultado/divisor;   
             
             			if(mostrar<1 && mostrar>0){
                       			divisor=divisor/10;                             
             			}
	             		else{
					uart_putchar(mostrar+48);
                  			resultado=resultado-mostrar*divisor;
                  			divisor=divisor/10;     
             			}
             			if(divisor==1){
                        	    	uart_putchar(resultado+48);
                        	    	i=10;					
                		}
             
  			} 
			else{
				i=10;
			}    

		}
		
		uart_putstr("\r\nOperación terminada\r\n");
		
	}
	
}
*/

#include "soc-hw.h"


//Funiones principales
//Calcular la humedad relativa con el sensor HSM-20G
int32_t h_relativa(int32_t x){
    //return 0.376*(powf(x,3))-0.704*(powf(x,2))+28.829*x-8.697;
    return 3710*(x*x*x)-20652*(x*x)+6480*x-27437;
}



//Funciones de envio y recepción de datos
uint32_t recibir_d16(){
    int32_t r;
    uint8_t h, l;
    h=uart0_getchar();
    l=uart0_getchar();
    r=(int32_t)h; 
    r=r<<8; r=r+l;
    return r;
}

void enviar_d16(int32_t x){
    uint8_t h, l;
    h=x>>8;
    l=x;
    uart1_putchar(h);
    uart1_putchar(l);
}

uint32_t stepb(int x){
    switch(x){
    case 0: return 5;
        break;
    case 1: return 9;
        break;
    case 2:return 10;
        break;
    case 3: return 6;
        break;
    default: break;
    }
}

uint32_t step8(int y){
        switch(y){
    case 0: return 1;
        break;
    case 1: return 5;
        break;
    case 2: return 4;
        break;
    case 3: return 6;
        break;
    case 4: return 2;
        break;
    case 5:return 10;
        break;
    case 6: return 8;
        break;
    case 7: return 9;
        break;
    default: break;
    }
}

/*
int main(int argc, char **argv){

	uint32_t x = 0;
	int	i = 0;
	for(;;){
		x=readGpio();
		if(x == 1){	
		    writeGpio(stepb(i));
			sleep(3);
			i++;
			if(i==4){
				i=0;
			}
		}
		else if(x==2){
			writeGpio(stepb(i));
			sleep(3);
			i--;
			if(i==-1){
				i=3;
			}
		}
		else if (x==0){
			writeGpio(0);
		} 
	}
}
*/

int main(int argc, char **argv){

	uint32_t x = 0;
	int	i = 0;
	for(;;){
		x=readGpio();
		if(x == 1){	
		    writeGpio(step8(i));
			sleep(1);
			i++;
			if(i==8){
				i=0;
			}
		}
		else if(x==2){
			writeGpio(step8(i));
			sleep(1);
			i--;
			if(i==-1){
				i=7;
			}
		}
		else if (x==0){
			writeGpio(0);
		} 
	}
}
