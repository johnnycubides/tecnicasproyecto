#include "soc-hw.h"
/*
Variable de 32 bits
0	-> Servo Motor	
1	-> Ventilador
2	-> Motobomba
3	-> luces
4	-> luces
5	-> Luces
6	-> Luces
7	-> Selector1
8	-> Selector2
9	-> Selector3
10	-> Eneable ADC
*/

uint8_t conversor(uint8_t selector, uint32_t gpio);


uint8_t conversor(uint8_t selector, uint32_t gpio){
	uint32_t canal;	
	uint32_t eneable=1024;
	uint32_t dato;

	canal = selector*128;
	writeGpio(gpio+canal);
	sleep(20);
	writeGpio(gpio+canal+eneable);
	sleep(20);
	writeGpio(gpio+canal);
	sleep(20);
	dato = readGpio();
	dato=dato>>2;
	return (uint8_t)(dato);
}


int main(int argc, char **argv){
	
	char opcion;
	uint8_t i;
	uint8_t adc8=0;
	uint32_t GPIO32=0;
	uint32_t ventilador=0;
	uint32_t bomba=0;
	uint32_t leds=0;  	 

	do{
	//uart0_putstr("\r\na y b parasol, c y d ventilador\r\n");//**
	//uart0_putstr("e y f bomba, (1,2,3,4) luces\r\n");//**
    //uart0_putstr("Opcion:\r\n");//**

    opcion = uart0_getchar();
	//uart0_putchar(opcion);//**
         
	switch(opcion){
	case 'a': GPIO32=ventilador+bomba+leds;
		for(i=0; i<100; i++){
			writeGpio(GPIO32+1);
                       	sleep(1);
                        writeGpio(GPIO32);
			sleep(19);
		}
	break;
	case 'b': GPIO32=ventilador+bomba+leds;	
                for(i=0; i<100; i++){
                writeGpio(GPIO32+1);
                sleep(2);
                writeGpio(GPIO32);
                sleep(18);
                }
	break;
	case 'c':
	ventilador=2;
	GPIO32=ventilador+bomba+leds;
	writeGpio(GPIO32);	
	break;
	
	case 'd':
	ventilador=0;
	GPIO32=ventilador+bomba+leds;
	writeGpio(GPIO32);
	break;
	
	case 'e':
	bomba=4;
	GPIO32=ventilador+bomba+leds;
	writeGpio(GPIO32);
	break;
	
	case 'f':bomba=0;
	GPIO32=ventilador+bomba+leds;
	writeGpio(GPIO32);
	break;
	case '1': leds=0;
	GPIO32=ventilador+bomba+leds;
	writeGpio(GPIO32);
	break;
	
	case '2': leds=40;
	GPIO32=ventilador+bomba+leds;
	writeGpio(GPIO32);
	break;
	
	case '3': leds=80;
	GPIO32=ventilador+bomba+leds;
	writeGpio(GPIO32);
	break;	
	
	case '4': leds=120;
	GPIO32=ventilador+bomba+leds;
	writeGpio(GPIO32);
	break;
	
	case '5':
		for(i=0; i<3; i++){
			adc8=conversor(i, GPIO32);
			uart0_getchar();
			uart0_putchar(adc8);
		}
	break;
	default:
	//uart0_putstr("\r\n Opcion Invalida\r\n");//**
	uart0_putchar('0');
	break;	
	}
	}while(1);
}
