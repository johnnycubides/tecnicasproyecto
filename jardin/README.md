# Jardin #

Éste directorio contiene los Makefile, los archivos verilog
y demás para sintetizar el proyecto e implementarlo en una FPGA spartan2

El programa del procesador debe ser escrito en el directorio siguiente

```
./jardin/firmware
```
Allí como era de esperarse existen varios ejemplos que se pueden usar para hacer el proyecto
```
:firware$ ls
:firware$ boot0-serial  cain_loader  ddr-phaser  gdb-test  hw-test  proyecto  pruebaUart
```
si se decide crear un nuevo directorio dentro de *firmware* no olvide enlazarlo dentro del
archivo **system.v**

Ejemplo del contenido de éste archivo **system.v**

```
:jardin$ less system.v

//---------------------------------------------------------------------------
// LatticeMico32 System On A Chip
//
// Top Level Design for the Xilinx Spartan 3-200 Starter Kit
//---------------------------------------------------------------------------

module system
#(
//      parameter   bootram_file     = "../firmware/cain_loader/image.ram",
//      parameter   bootram_file     = "../firmware/boot0-serial/image.ram",
//      parameter   bootram_file     = "../firmware/proyecto/image.ram",
        parameter   bootram_file     = "../firmware/pruebaUart/image.ram",
        parameter   clk_freq         = 50000000,
        parameter   uart_baud_rate   = 9600
) ( ...CONTINÚA
```
como se pude ver, el archivo a montar es la imagen creada del firmware/pruebaUart/image.ram,

## Makefile ##

El *Makefile* contenido en el nivel del directorio jardín es el encargado de construir y sintetizar.
Secuencia ordinaria usada para sintetizar en una FPGA teniendo Xilin instalado y una FPGA:

```
make clean
make
make syn
```

Aclaración:

make clean	-> limpia una sintetización anterior.

make		-> Construye el programa que ejecutará el procesador.

make syn	-> Sintetiza con la herramienta Xilin el codigo verilog para una FPGA


