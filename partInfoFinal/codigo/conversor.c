\begin{verbatim}
uint8_t conversor(uint8_t selector, uint32_t gpio){
	uint32_t canal;	
	uint32_t eneable=1024;
	uint32_t dato;

	canal = selector*128;
	writeGpio(gpio+canal);
	sleep(20);
	writeGpio(gpio+canal+eneable);
	sleep(20);
	writeGpio(gpio+canal);
	sleep(20);
	dato = readGpio();
	dato=dato>>2;
	return (uint8_t)(dato);
}
\end{verbatim}